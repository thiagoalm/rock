import { Component, OnInit, NgModule} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalBasicComponent } from '../modal-basic/modal-basic.component';

@Component({
  selector: 'app-content-table',
  templateUrl: './content-table.component.html',
  styleUrls: ['./content-table.component.sass']
})

@NgModule({
  imports: [
    ModalBasicComponent
  ],
  declarations: [
    ModalBasicComponent
  ],
})

export class ContentTableComponent implements OnInit {

  functions = [
    {
      description: 'Freela',
      value: 'frela',
    },
    {
      description: 'Usuario',
      value: 'user',
    }
  ]
  team = [
    {
      name: 'Usuário A.',
      email: 'usuario.a@dominio.com.br',
      type: 'frela',
      photo: 'assets/images/photos/frelas.jpg',
    },
    {
      name: 'Usuário B.',
      email: 'usuario.b@dominio.com.br',
      type: 'user',
      photo: 'assets/images/photos/users.jpg',
    },
    {
      name: 'Usuário C.',
      email: 'usuario.c@dominio.com.br',
      type: 'user',
      photo: 'assets/images/photos/users.jpg',
    },
    {
      name: 'Usuário D.',
      email: 'usuario.d@dominio.com.br',
      type: 'frela',
      photo: 'assets/images/photos/frelas.jpg',
    },
  ]
  
  public dispalayBlock: boolean;
  public dispalayOptionCreate: boolean;
  public dispalayOptionUpdate: boolean;
  public valueName: string;
  public valueMail: string;
  public valueFunction: string;
  public detailText: string;
  textName: string = '';
  textEmail: string = '';
  public globalIndex: number;

  infoName(event: any) {
    console.log(event.target.value)
    this.valueName = event.target.value;
  }
  infoMail(event: any) {
    this.valueMail = event.target.value;
  }
  infoFunction(event: any) {
    this.valueFunction = event.value;
  }

  constructor(
    private modal: NgbModal
  ){ 
    
  }

  ngOnInit() {
    this.dispalayBlock = false;
    this.dispalayOptionCreate = false;
    this.dispalayOptionUpdate = false;
  }
  
  public openBoxNewMember(value, index){
    if(value || value == 0){
      this.dispalayBlock = true;
      this.dispalayOptionUpdate = true;
      this.detailText = "Edição de Membro";
      console.log(this.team)
      this.valueName = this.team[index].name;
      this.valueMail = this.team[index].email;
      this.textName = this.team[index].name;
      this.textEmail = this.team[index].email;
      this.globalIndex = index
    }else{
      let item = {
        name: this.valueName,
        email: this.valueMail,
        type: this.valueFunction,
        photo: 'assets/images/photos/' + this.valueFunction + 's.jpg',
      };
      this.textName = ""
      this.textEmail = ""
      this.dispalayBlock = true;
      this.dispalayOptionCreate = true;
      this.detailText = "Novo Membro";
    }
    
  }

  public closeBoxNewMember(){
    this.dispalayBlock = false;
    this.dispalayOptionCreate = false;
    this.dispalayOptionUpdate = false;
  }

  // TODO: Refatorar para método declarativo
  member(type): void {
    let item = {
      name: this.valueName,
      email: this.valueMail,
      type: this.valueFunction,
      photo: 'assets/images/photos/' + this.valueFunction + 's.jpg',
    };
    console.log(item)
    if(type == "create"){
      this.team.push(item);
      this.closeBoxNewMember();
      this.modal.open("Cadastro realizada com sucesso!");
    }else if(type == "update"){
      this.team[this.globalIndex ] = item;
      this.closeBoxNewMember();
      this.modal.open("Edição realizada com sucesso!");
    }
    console.log(this.team)
    
  }

}
