import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-users',
  templateUrl: './card-users.component.html',
  styleUrls: ['./card-users.component.sass']
})
export class CardUsersComponent implements OnInit {

  @Input() player: Element;

  constructor() { }

  ngOnInit() {
  }

}
