import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContentsComponent } from './pages/contents/contents.component';
import { TasksComponent } from './pages/tasks/tasks.component';
import { SetupComponent } from './pages/setup/setup.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/setup',
    pathMatch: 'full',
  },
  {
    path: 'tasks',
    component: TasksComponent,
  },
  {
    path: 'contents',
    component: ContentsComponent,
  },
  {
    path: 'setup',
    component: SetupComponent,
  },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule { }
