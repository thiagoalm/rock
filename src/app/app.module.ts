import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CardUsersComponent } from './components/card-users/card-users.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatTabsModule, MatInputModule, MatSelectModule} from '@angular/material';

import { ContentTableComponent } from './components/content-table/content-table.component';
import { MenuLeftComponent } from './components/menu-left/menu-left.component';
import { HeaderComponent } from './components/header/header.component';
import { DropdownHeaderComponent } from './components/dropdown-header/dropdown-header.component';
import { AppRoutingModule } from './/app-routing.module';
import { RouterModule, Routes } from '@angular/router';
import { ContentsComponent } from './pages/contents/contents.component';
import { TasksComponent } from './pages/tasks/tasks.component';
import { SetupComponent } from './pages/setup/setup.component';
import { ModalBasicComponent } from './components/modal-basic/modal-basic.component';


@NgModule({
  declarations: [
    AppComponent,
    CardUsersComponent,
    ContentTableComponent,
    MenuLeftComponent,
    HeaderComponent,
    DropdownHeaderComponent,
    ContentsComponent,
    TasksComponent,
    SetupComponent,
    ModalBasicComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule,
    AppRoutingModule,
    RouterModule.forRoot([]),
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
